import { FormGroup, AbstractControl } from '@angular/forms';
import * as EWIS from './EWIS/_import';
import { Observable } from 'rxjs';

export { };
type MaybeExtendable<T> = T extends new (...args) => infer U ? T : never;

declare module '@angular/router/src/shared' {
    interface ParamMap {
        has<P extends string>(name: P): this is this & { get(name: P): string };
    }
}


declare module '@angular/forms/src/form_builder' {
    interface FormBuilder {
        group<K extends string, V  =  V extends Array<infer U> ? Array<U> : V>(controlsConfig: {
            [P in K]: any;
        }, extra?: {
            [key: string]: any;
        }): {
            controls: { [P in K]: AbstractControl },
            value: { [P in K]: any }
        } & Pick<FormGroup, Exclude<keyof FormGroup, 'value'>>;
    }
}

declare module 'rxjs/internal/observable/fromEvent' {
    export function fromEvent<K extends keyof WindowEventMap>(target: FromEventTarget<WindowEventMap[K]>, eventName: K): Observable<WindowEventMap[K]>;
    export function fromEvent<K extends keyof WindowEventMap>(target: FromEventTarget<WindowEventMap[K]>, eventName: K, options: EventListenerOptions): Observable<WindowEventMap[K]>;
}

declare global {
    interface Window {
        dataLayer: any[];
        LO: { new_page(title: string): void };
        __wtw_lucky_override_save_url: string;
    }

    interface Storage {
        /**
         * A serialized represenation of the authenticated `User`
         */
        easyPayUser?: string;
        /**
         * A serialized represenation of the authenticated `User`
         */
        user?: string;
        store?: string;
        workstation?: string;

        customer?: string;
        application?: string;
        Contract?: string;
        loanRequest?: string;
        step: string;
        rolloverData?: string;
    }

    interface Element {
        hasAttribute<A extends string>(
            qualifiedName: A
        ): this is Overwrite<this, 'getAttribute', (qualifiedName: A) => string>;
    }

    interface StorageEvent {
        windowHash?: string;
    }

    type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

    type Overwrite<T, K extends keyof T, V> = Omit<T, K> & { [P in K]: V };
    type Replace<T, R = R extends { [P in keyof T] } ? R : never, K extends keyof R = keyof R> = T & { [P in K]: R[P] };
}
