﻿export default Object.freeze({
  endpointBaseUrl: 'https://beta-api.minlc.com/api',
  prod: true,
  verbose: undefined
});
