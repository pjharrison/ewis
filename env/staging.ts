export default Object.freeze({
    endpointBaseUrl: 'https://stagingesp.minlc.com/API/api',
    prod: false,
    verbose: undefined
});