import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import path from 'path';
import webpack from 'webpack';
/**
 * @type {({dev = false, prod = false, staging = false}) => webpack.Configuration}
 */
export default function ({ dev = false, staging = false, prod = false, local = false } = {}) {
    return {
        resolve: {
            extensions: ['.ts', '.js'],
            alias: {
                env: path.join(
                    __dirname,
                    `./env/${dev ? 'dev' : staging ? 'staging' : local ? 'local' : 'prod'}`
                )
            },
            modules: ['node_modules', 'src/scan']
        },
        devtool: dev ? 'cheap-module-eval-source-map' : undefined,
        module: {
            rules: [
                {
                    test: /\.js$/,
                    include: [path.join(path.resolve('node_modules'), 'dwt'), /dynamsoft/],
                    loader: 'script-loader'
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader'
                },
                {
                    test: /\.ts$/,
                    loader: [
                        {
                            loader: 'awesome-typescript-loader',
                            options: {
                                transpileOnly: true,
                                configFileName: prod
                                    ? 'tsconfig.prod.json'
                                    : 'tsconfig.json'
                            }
                        },
                        'angular-router-loader',
                        'angular2-template-loader'
                    ]
                },
                {
                    test: /\.html$/,
                    loader: 'raw-loader'
                },
                {
                    test: /\.css$/,
                    exclude: /src/
                },
                {
                    test: /\.css$/,
                    use: 'raw-loader'
                },
                {
                    test: /.less$/,
                    use: 'less-loader'
                }
            ]
        },
        optimization: {
            minimizer: [
                new UglifyJsPlugin()
            ]
        },
        plugins: [
            new CleanWebpackPlugin('wwwroot/*'),
            new CleanWebpackPlugin('wwwroot/**.*'),
            new CopyWebpackPlugin([{ from: 'contract', to: 'contract' }]),
            new CopyWebpackPlugin([{ from: 'assets', to: 'assets' }]),
            new CopyWebpackPlugin([
                { from: 'src/scan/resources/', to: 'resources' }
            ]),
            new CopyWebpackPlugin([
                { from: 'node_modules/dwt/dist', to: 'resources' }
            ]),
            new HtmlWebpackPlugin({
                template: 'index.html',
                favicon: 'favicon.ico'
            })
        ],
        entry: './src/index.ts',

        output: {
            publicPath: '/',
            filename: '[name].[chunkhash].js',
            chunkFilename: '[name].[chunkhash].js',
            path: path.resolve(__dirname, 'wwwroot')
        },

        mode: dev ? 'development' : 'production'
    };
};
