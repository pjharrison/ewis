import CustomerReferenceType from './Types/CustomerReferenceType';
import BankAccountVerificationType from './Types/BankAccountVerificationType';
import LoanAutoPayType from './Types/LoanAutoPayType';
import LoanType from './Types/LoanType';
import DocumentStatusType from './Types/DocumentStatusType';
import EmploymentType from './Types/EmploymentType';
import CustomerStatusType from './Types/CustomerStatusType';
import PettyCashType from './Types/PettyCashType';
import CheckStatus from './Types/CheckStatus';
import IncomeType from './Types/IncomeType';
import BankAccountType from './Types/BankAccountType';
export interface TypesModel {
    CheckStatuses: CheckStatus[];
    PettyCashes: PettyCashType[];
    CustomerStatuses: CustomerStatusType[];
    Incomes: IncomeType[];
    Employments: EmploymentType[];
    CustomerReferences: CustomerReferenceType[];
    BankAccounts: BankAccountType[];
    BankAccountVerifications: BankAccountVerificationType[];
    AutoPays: LoanAutoPayType[];
    Loans: LoanType[];
    DocumentStatuses: DocumentStatusType[];
}
