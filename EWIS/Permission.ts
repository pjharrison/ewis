export default Permission;
type Permission = keyof typeof Permissions;
export enum Permissions {
    "Administrator",
    "Instructor", "Teacher" = Permissions.Instructor,
    "Guardian", "Parent" = Permissions.Guardian,
    "Student", "Kid" = Permissions.Student
}
export function normalizeRole(role: Permission) {
    switch (role) {
        case "Teacher":
            return "Instructor";
        case "Parent":
            return "Guardian";
        case "Kid":
            return "Student";
        default:
            return role;
    }
}