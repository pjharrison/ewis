﻿
import DocumentTemplate from './DocumentTemplate';
import DocumentStatus from './DocumentStatus';
import Model from './Model';
import Signature from './signature';

export default Document;

export interface Document extends Model { 
    TemplateID?: number;
    Template?: DocumentTemplate;
    Html?: string;
    jsonSignatures?: string;
    Signatures?: { [key: string]: Signature; };
    Statuses?: DocumentStatus[];
    Status?: DocumentStatus;
}