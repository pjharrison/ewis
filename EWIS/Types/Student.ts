﻿import Person from './Person';
import User from './User';
import Model from './Model';
import StudentStatus from './StudentStatus';
import StudentReference from './StudentReference';
import StudentNote from './StudentNote';

export default Student;

export interface Student extends Model {
    ID?: number;
    Person?: Person;
    UserID?: number;
    User?: User;
    Date?: Date;
    Statuses?: StudentStatus[];
    Status?: StudentStatus;
    References?: StudentReference[];
    Notes?: StudentNote[];
}