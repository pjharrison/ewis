export default function scaleMonetaryValue(value: number, factor: number) {
    const normalizedValue = Math.floor(value * 100);
    const scaled = normalizedValue * factor;
    return Number(scaled.toFixed(4)) / 100;
}
