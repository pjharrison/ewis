import { Observable } from "rxjs";
import { ParamMap } from "@angular/router";
import { map } from "rxjs/operators";
import extractParameter from "./extract-parameter";

const extractNumericParameter = <K extends string>(key: K) => (source: Observable<ParamMap>) => source.pipe(
    extractParameter(key),
    map(Number)
);

export default extractNumericParameter;