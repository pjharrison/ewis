import { Observable } from "rxjs";
import { ParamMap } from "@angular/router";
import { map, filter } from "rxjs/operators";

const extractParameter = <K extends string>(key: K) => (source: Observable<ParamMap>) => source.pipe(
    filter(paramMap => paramMap.has(key)),
    map(paramMap => <string>paramMap.get(key))
);

export default extractParameter;