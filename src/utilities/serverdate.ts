﻿export default function serverdate() {
    var utc = new Date(new Date().getTime() + new Date().getTimezoneOffset() * 60000);
    var jan = new Date(new Date().getFullYear(), 0, 1);
    var jul = new Date(new Date().getFullYear(), 6, 1);
    var isDST = Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset()) == new Date().getTimezoneOffset();
    var offset = -5;
    if (isDST)
        offset--;

    var value = new Date(utc.getTime() + (offset * 60000 * 60));
    value.setHours(0, 0, 0, 0);
    return value;
}