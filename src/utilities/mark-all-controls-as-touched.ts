import { AbstractControl } from '@angular/forms';

export default function markAllControlsAsTouched(form: { controls: { [formControlName: string]: AbstractControl } }) {
  Object.values(form.controls).forEach(control => {
    if (control) control.markAsTouched({ onlySelf: true });
  });
}
