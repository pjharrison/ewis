﻿/** 
 * let transaction of Transactions | sort: [{ key: 'city', direction: 'asc' }, { key: 'price', direction: 'desc' }]; 
 * */
export default function sort<T>(value: Array<T>, args?: Array<{ key: keyof T, direction: "asc" | "desc" }>) {
    return value.slice().sort(function (a, b) {
        for (var i = 0; args && i < args.length; i++) {
            let result = a[args[i].key] < b[args[i].key] ? -1 : a[args[i].key] > b[args[i].key] ? 1 : 0;
            if (args[i].direction == "desc")
                result = result * -1;
            if (result !== 0)
                return result;
        }
        return 0;
    })
}