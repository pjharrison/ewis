import Guards from '../tokens/guards.token';
import LoggedInGuard from '../guards/logged-in.guard';

export default [
    { provide: Guards, useClass: LoggedInGuard, multi: true }
];
