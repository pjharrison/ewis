import env from 'env';

export interface EndPoint { method: string; controller: string; }
export async function post(type: string, endPoint: EndPoint, body: object): Promise<any>;
export async function post(endPoint: EndPoint, body: object): Promise<any>;
export async function post(typeOrEndPoint: string | EndPoint, endPointOrBody: EndPoint | object, body?: object) {
    // tslint:disable-next-line:no-unnecessary-initializer spurious warning
    const { controller = undefined, method = undefined } = typeof typeOrEndPoint !== 'string' ? typeOrEndPoint : 'method' in endPointOrBody ? endPointOrBody : {};
    const type = typeof typeOrEndPoint === 'string' ? typeOrEndPoint : undefined;
    const postBody = body ? body : endPointOrBody;
    const response = await makeRequest(controller, method, postBody, type, "POST");

    return await processResponse(response);
}

async function processResponse(response: Response) {
    if (response.ok) {
        const data = await response.json();
        if (env.verbose) {
            if (data.Result)
                console.info('Result: ', data.Result);
            if (data.Error)
                console.info('Error: ', data.Error);
        }
        if (data.Error && !data.Result) {
            throw Error(data.Error.Message || data.Error.Description || data.Error.Name || data.Error);
        }
        return data.Result;
    } else {
        let data = {};
        // Attempt to extract detailed errors in JSON format with no expectation of success.
        try { data = response.json(); } catch { } //
        throw { status: response.status, message: response.statusText, data };
    }
}

async function makeRequest(controller?: string, method?: string, postBody?: object, type?: string, httpMethod?: "GET" | "POST" | "PUT" | "PATCH") {
    if (env.verbose)
        console.log(JSON.stringify({ ...postBody, Method: method, Controller: controller, Type: type ? type.startsWith('MLC.Data.Models.') ? type : `MLC.Data.Models.${type}` : undefined }));
    let headers: HeadersInit = {
        "Content-Type": "Application/JSON"
    };
    const token = getToken();
    const store = getStore();
    if (token) headers = { ...headers, User: token };
    if (store) headers = { ...headers, Store: store };
    let request: RequestInit = {
        method: httpMethod,
        headers
    };
    if (postBody && method !== "GET") {
        request = {
            ...request,
            body: JSON.stringify({
                ...postBody, Method: method,
                Controller: controller,
                 Type: type
                    ? type.startsWith('MLC.Data.Models.') ? type
                        : `MLC.Data.Models.${type}` : undefined
            })
        };
    }
    return await fetch(`${env.endpointBaseUrl}/${controller}/${method}`, request);
}
export async function get(endPoint: string | EndPoint) {
    const { controller, method } = typeof endPoint === 'string' ? { method: undefined, controller: undefined } : endPoint;
    const response = await makeRequest(controller, method, undefined, undefined, "GET");

    return await processResponse(response);
}

function getUser() {
    const userJson = localStorage.user;
    if (userJson) {
        const user = JSON.parse(userJson);
        return user;
    }
}
function getToken() {
    const user = getUser();
    if (user)
        return user.Token;
}
function getStore() {
    const storeJson = localStorage.store;
    if (storeJson)
        return JSON.parse(storeJson).ID;
}

export type Method = "ByID" | "Signin";
export type Controller = "User" | "Model";
