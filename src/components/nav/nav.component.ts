import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Global } from '../../services/global';


@Component({
    selector: 'nav-menu',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.css'],
    animations: [
        trigger('collapse', [
            state(
                'open',
                style({
                    opacity: '1'
                })
            ),
            state(
                'closed',
                style({
                    opacity: '0',
                    display: 'none'
                })
            ),
            transition('closed => open', animate('1000ms ease-in')),
            transition('open => closed', animate('1000ms ease-out'))
        ])
    ]
})
export default class NavComponent {
    navbarCollapsedAnimation?: string;
    navbarCollapsed = true;
    constructor(public global: Global, readonly router: Router) { }

    show: boolean = true;
    alternate: boolean = false;
    glance: any = {};

    ngOnInit() {
        this.onResize(window);

        if (window.location.hash.indexOf("print") !== -1) {
            this.show = false;
            this.alternate = false;
        }
        if (this.show) {
            this.setGlance();
            setInterval(() => this.setGlance(), 30000);
        }
    }
    async setGlance() {
        if (this.global.User) {
            ////////// get glance counts \\\\\\\\\\
            //Messages:
            const request = { User: this.global.User };
            const msg = await search("Announcement", request);
            this.glance = { Messages: msg.length };
        }
    }

    @HostListener('window:resize', ['$event.target'])
    onResize(event) {
        if (event.innerWidth > 990) {
            //need to set this to 'open' for large screens to show up because of opacity in 'closed' animation.
            this.navbarCollapsedAnimation = 'open';
            this.navbarCollapsed = true;
        } else {
        }
    }
    toggleNavbar() {
        if (this.navbarCollapsed) {
            this.navbarCollapsedAnimation = 'open';
            this.navbarCollapsed = false;
        } else {
            this.navbarCollapsedAnimation = 'closed';
            this.navbarCollapsed = true;
        }
    }
    get isNavbarCollapsedAnim() {
        return this.navbarCollapsedAnimation;
    }
}
