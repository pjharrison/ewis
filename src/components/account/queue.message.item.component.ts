﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Global } from '../../services/global';

import MessageService from '../../services/message.service';


@Component({
    selector: 'queuemessageitem',
    templateUrl: './queue.message.item.component.html',
    styleUrls: ['./queue.message.item.component.css']
})
export default class QueueMessageItemComponent {
    constructor(readonly global: Global, readonly router: Router, , readonly messageService: MessageService) { }
    processing: boolean = false;
    loading: boolean = false;
    get item() { return this.messageService.item; }

    async ngOnInit() {
        if (!this.item) {
            this.router.navigate(['queuemessage']);
            return;
        }
        if (this.item.To && this.item.To.ID === this.global.userID && this.item.Status && this.item.Status.Name === "Unread") {
            this.item.Status.Name = "Read";
            await this.models.save("Announcement", this.item);
        }
    }
    back() {
        this.router.navigate(['queuemessage']);
        return;
    }
    reply() {
        this.compose("reply");
        this.router.navigate(['queuemessagecompose']);
        return;
    }
    forward() {
        this.compose("forward");
        this.router.navigate(['queuemessagecompose']);
        return;
    }
    compose(type: string) {
        if (this.item) {
            this.messageService.compose = {};
            this.messageService.compose.From = this.global.User;
            switch (type) {
                case "reply":
                    this.messageService.compose.To = [];
                    this.messageService.compose.To.push(this.item.From);
                    this.messageService.compose.Subject = `RE: ${this.item.Subject}`;
                    this.messageService.compose.Body = `<br /><hr /><b>FROM:</b>${this.item.From.FirstName} ${this.item.From.LastName}<br /><b>DATE:</b>${this.item.Date}<br /><b>SUBJECT:</b>${this.item.Subject}<br /><br />${this.item.Body}`;
                    break;
                case "forward":
                    this.messageService.compose.Subject = `FW: ${this.item.Subject}`;
                    this.messageService.compose.Body = `<br /><hr />
                                                        <b>FROM:</b>${this.item.From.FirstName} ${this.item.From.LastName}
                                                        <br />
                                                        <b>DATE:</b>${this.item.Date}
                                                        <br />
                                                        <b>SUBJECT:</b>${this.item.Subject}
                                                        <br /><br />
                                                        ${this.item.Body}`;
                    break;
            }
        }
    }
}