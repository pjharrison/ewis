import { Component, ViewChild } from '@angular/core';
import { BaseChartDirective } from 'ng2-charts';

import * as EWIS from '../../../EWIS/_import';
import UserService from '../../services/user.service';

@Component({
    selector: 'homestats',
    templateUrl: './home.stats.component.html',
    styleUrls: ['./home.stats.component.css']
})
export default class HomeStatsComponent {
    constructor(readonly userService: UserService) { }

    @ViewChild("statChart") statChart?: BaseChartDirective;
    loading: boolean = false;
    stats?: Array<EWIS.Types.Performance>;
    stat?: EWIS.Types.Performance;
    activeStat?: string;
    chartOptions = { Type: 'line', Legend: true };

    async ngOnInit() {
      await this.getStats();
    }

    async getStats() {
        this.loading = true;

        this.stats = await this.userService.performance();
        if (!this.stat && this.stats)
            this.stat = this.stats[0];

        this.activeStat = this.stat && this.stat.Name;

        this.loading = false;
    }

    isFirst = true;
    async UpdateStatChart(stat) {
        if (!stat)
            return;
        this.activeStat = stat.Name;
        if (this.stat) {
            if (this.stat.Name === stat.Name && !this.isFirst)
                return;
        }

        this.stat = stat;
        if (this.statChart && this.stat) {
            this.statChart.ngOnDestroy();
            this.statChart.datasets = this.stat.Data || [];
            this.statChart.labels = this.stat.Label || [];
            this.statChart.options = this.chartOptions;
            this.statChart.colors = this.stat.ChartColor;
            this.statChart.chartType = this.chartOptions.Type;
            this.statChart.legend = this.chartOptions.Legend;
            this.statChart.chart = this.statChart.getChartBuilder(this.statChart.ctx);
        }
        this.isFirst = false;
    }
}