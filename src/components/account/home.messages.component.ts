import { Component } from '@angular/core';
import { Global } from '../../services/global';

import * as EWIS from '../../../EWIS/_import';
import UserService from '../../services/user.service';

@Component({
    selector: 'homemessages',
    templateUrl: './home.messages.component.html',
    styleUrls: ['./home.messages.component.css']
})
export default class HomeMessagesComponent {
    constructor(readonly global: Global, readonly userService: UserService) { }

    loading: boolean = false;
    glances: Array<EWIS.Types.Glance> = [];

    async ngOnInit() {
        this.loading = true;

        this.glances = await this.userService.glances(this.global.User, this.global.Store);

        this.loading = false;
    }
}