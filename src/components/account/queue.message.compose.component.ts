import { Component, Inject, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import ValidationService from '../../shared/services/validation.service';
import { Global } from '../../services/global';

import * as EWIS from '../../../EWIS/_import';
import MessageService from '../../services/message.service';


@Component({
    selector: 'queuemessagecompose',
    templateUrl: './queue.message.compose.component.html',
    styleUrls: ['./queue.message.compose.component.css'],
    host: { '(document:click)': 'handleClick($event)' }
})
export default class QueueMessageComposeComponent {
    constructor(readonly global: Global, readonly router: Router, , readonly messageService: MessageService, readonly formBuilder: FormBuilder, readonly validationService: ValidationService, @Inject(ElementRef) readonly elementRef: { nativeElement: Element }) { }
    processing = false;
    loading = false;
    searching = false;

    toHeight = 38;
    sendTo: Array<EWIS.Types.User> = this.messageService.compose && this.messageService.compose.To || [];
    subject: string = this.messageService.compose && this.messageService.compose.Subject || "";
    body = "";

    employees: Array<EWIS.Types.Employee> | undefined;

    __filter = "";
    get Filter(): string { return this.__filter; }
    set Filter(value: string) { this.__filter = value; this.Search(); }

    composeForm = this.formBuilder.group({
        subject: ['', Validators.required]
    });

    ngOnInit() {
        console.log(this.messageService.compose && this.messageService.compose.Body);
    }

    async Search() {
        this.searching = true;
        this.employees = undefined;
        if (this.sendTo && this.sendTo.length > 0) {
            if ((this.sendTo.length) % 5 === 0 && this.Filter === "") {
                this.toHeight += 30;
            }
        }

        if (!this.Filter) {
            this.searching = false;
            return;
        }

        if (this.Filter.length > 3) {
            this.employees = await quickSearch("Employee", this.Filter);
            if (this.employees && this.sendTo)
                this.employees = this.employees.filter(x => !this.sendTo.some(y => x.ID === y.ID));
        }
        this.searching = false;
    }
    SetActive(event) {
        var target: Element | null;
        var node = window.document.querySelector("li.search-active");
        if (node !== null)
            node.className = "customer";

        target = event.target || event.srcElement || event.currentTarget;
        if (target) {
            if (event.type === "mouseover")
                target.className = "search-active customer";
            else
                target.className = "customer";
        }
    }
    handleClick(event) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement)
                inside = true;
            clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if (!inside)
            this.ClearList();
    }
    ClearList() {
        this.employees = undefined;
        this.Filter = "";
    }
    AddEmployee(employee: EWIS.Types.Employee) {
        if (!this.sendTo)
            this.sendTo = [];
        this.sendTo.push(employee);
        this.ClearList();
    }
    RemoveEmployee(employee: EWIS.Types.Employee) {
        this.sendTo = this.sendTo.filter(x => x.ID !== employee.ID);
    }
    ResetForm() {
        this.messageService.compose = undefined;
        this.composeForm.reset();
        this.sendTo = [];
        this.Filter = "";
        this.router.navigate(['queuemessage']);
    }
    async Send() {
        this.sendTo = this.sendTo.filter((thing, index, self) => self.findIndex(t => t.ID === thing.ID) === index);
        const send = async () => {
            for (const to of this.sendTo) {
                const msg: EWIS.Types.Announcement = {
                    Status: {
                        Name: "Unread"
                    },
                    To: (!to.Person) ? to : to.Person,
                    From: this.global.User && this.global.User.Person,
                    Subject: this.composeForm.value.subject,
                    Body: this.body
                };
                await this.models.save("Announcement", msg);
            }
            this.messageService.compose = undefined;
            this.router.navigate(['queuemessage']);
        };
        await send();
    }
}