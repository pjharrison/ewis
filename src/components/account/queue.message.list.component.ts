﻿import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Global } from '../../services/global';

import * as EWIS from '../../../EWIS/_import';
import MessageService from '../../services/message.service';


@Component({
    selector: 'queuemessagelist',
    templateUrl: './queue.message.list.component.html',
    styleUrls: ['./queue.message.list.component.css']
})
export default class QueueMessageListComponent {
    private __box: string = "Inbox";
    @Input() get Box(): string { return this.__box; }
    set Box(value: string) {
        if (this.__box != value) {
            this.__box = value;
            this.getMessages(this.__box, 1);
        }
    }

    constructor(readonly global: Global, readonly router: Router, , readonly messageService: MessageService) { }

    selected: boolean = false;
    processing: boolean = false;
    loading: boolean = false;
    page: number = 1;
    items: Array<EWIS.Types.Announcement> = [];
    itemsPerPage: number = 15;

    ngOnInit() {
        this.getMessages(this.Box, 1);
    }

    async getMessages(box: string, page: number) {
        this.processing = true;

        this.Box = box;
        this.page = page;

        let skip: number = (this.itemsPerPage * (page - 1));
        let take: number = this.itemsPerPage;

        let request = { User: this.global.User, Mailbox: box, Range: { Skip: skip, Take: take } };
        this.items = await this.models.search("Announcement", request);

        this.processing = false;
    }
    strip(html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }
    backPage() {
        if (this.page > 1 && this.Box) {
            this.page--;
            this.getMessages(this.Box, this.page);
        }
    }
    nextPage() {
        this.page++;
        this.Box && this.getMessages(this.Box, this.page);
    }

    toggleAll() {
        this.selected = !this.selected;
        this.items && this.items.forEach(item => item.Selected = this.selected);
    }
    toggle(item: EWIS.Types.Announcement) {
        let selected = !item.Selected;
        this.items && this.items.forEach(i => {
            if (item == i)
                i.Selected = selected
        });
    }
    
    async asyncForEach(array) {
        for (let i = 0; i < array.length; i++) {
            let item = array[i];
            if (item.Selected && item.Status) {
                item.Status.Name = "Archived";
                await this.models.save("Announcement", item);
            }
        }
    }
    archive() {
        //Only allow archive of inbox items
        if (this.Box == "Inbox" && this.items) {
            const archive = async () => {
                await this.asyncForEach(this.items);
                this.refresh();
            }
            archive();


            /* ALUAN:
               Do not use forEach, it doesn't wait for it to be done before going to the next entry.  
               This causes this.refresh() to be called prior to the iteration to complete.
            this.items.forEach(item => {
                if (item.Selected && item.Status) {
                    item.Status.Name = "Archived";
                    save("MLC.Data.Models.Announcement", item);
                }
            });
            this.refresh();
            this.processing = false;
            */
        }
        
    }
    refresh() {
        this.selected = false;
        this.getMessages(this.Box, this.page);
    }
    readItem(item: EWIS.Types.Announcement) {
        this.messageService.item = item;
        this.router.navigate(['queuemessageitem']);
    }
}