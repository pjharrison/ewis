import { Component, ElementRef, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Global } from '../../../services/global';


@Component({
    selector: 'search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
    host: { '(document:click)': 'handleClick($event)' }
})
export default class SearchComponent {
    constructor(
        readonly global: Global,
        readonly router: Router,
        readonly activatedRoute: ActivatedRoute,
        readonly formBuilder: FormBuilder,
        @Inject(ElementRef) readonly elementRef: { nativeElement: Element }
    ) { }
    students?: Array<EWIS.Types.Student>;
    loading: boolean = false;
    SearchFilter?: string;

    __filter?: string;
    get filter(): string | undefined { return this.__filter; }
    set filter(value: string | undefined) {
        if (value == null) {
            this.students = undefined;
            this.__filter = undefined;
        } else {
            if (value != this.__filter) {
                this.__filter = value;
                this.Search();
            }
        }
    }

    //////////////////// Methods \\\\\\\\\\\\\\\\\\\\
    NewStudent() { this.router.navigate(['/create']); }
    async Search() {
        setTimeout(async () => {
            this.SearchFilter = this.filter;
            this.loading = true;
            this.students = await quickSearch("Customer", this.filter);
            this.loading = false;
        }, 650);
    }

    SetActive(event) {
        var node = window.document.querySelector("li.active");
        if (node != null)
            node.className = "customer";

        var target = event.target || event.srcElement || event.currentTarget;
        if (event.type == "mouseover")
            target.className = "active customer";
        else
            target.className = "customer";
    }
    ClearList() {
        this.students = undefined;
        this.filter = undefined;
    }
    async SelectStudent(ID: number) {
        this.ClearList();
        this.router.navigate(["/account", ID]);
        return;
    }
    keyboard(keycode) {
        if (keycode === 40 || keycode === 38) {
            var index = 0;
            var target = window.document.querySelector("li.active");
            if (target === null) {
                target = window.document.querySelector("li.customer");
                index--;
            }
            var node = target;
            while (node && (node = node.previousElementSibling))
                index++;

            if (target) target.className = "student";
            switch (keycode) {
                case 40: //Down Arrow
                    index++;
                    if (target && target.parentElement && index === (target.parentElement.children.length))
                        index = 0;
                    break;
                case 38: //Up Arrow
                    index--;
                    if (index < 0)
                        index = 0;
                    break;
            }

            target = target && target.parentElement && target.parentElement.children[index];
            if (target)
                target.className = "active";
        }
        if (keycode === 13) {
            //Enter key was struck:
            target = window.document.querySelector("li.active");
            if (target !== null && target.hasAttribute("key")) {
                this.SelectStudent(+target.getAttribute("key"));
            }
        }
    }
    handleClick(event: Overwrite<Event, 'target', { parentNode: Node | null } | null>) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement)
                inside = true;
            clickedComponent = clickedComponent && clickedComponent.parentNode;
        } while (clickedComponent);
        if (!inside)
            this.ClearList();
    }
}