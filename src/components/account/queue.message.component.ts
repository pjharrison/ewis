import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Global } from '../../services/global';

@Component({
    selector: 'queuemessages',
    templateUrl: './queue.message.component.html',
    styleUrls: ['./queue.message.component.css']
})
export default class QueueMessageComponent {
    constructor(readonly global: Global, readonly router: Router) { }

    processing: boolean = false;
    loading: boolean = false;
    box: string = "Inbox";

    ngOnInit() {

    }
}