import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import Person from '../../../MLC/types/Person';
import Bank from '../../../MLC/types/Bank';
import { IBVerificationStatus } from '../../../MLC/Types/IBVerificationStatus';
import TransientItemsService from '../../services/transient-items.service';
import { filter, map } from 'rxjs/operators';

import { Global } from '../../services/global';
import { PersonBankAccount } from 'MLC/Types/_import';
import markAllControlsAsTouched from '../../utilities/mark-all-controls-as-touched';

@Component({
    selector: 'bankform',
    templateUrl: './bank.form.component.html',
    styleUrls: ['./bank.form.component.css']
})
export default class BankFormComponent {
    get valid() {
        return this.form.valid;
    }
    @Input()
    isPayment = false;
    constructor(
        readonly global: Global,
        readonly router: Router,
        readonly route: ActivatedRoute,
        readonly location: Location,
        readonly formBuilder: FormBuilder,
        readonly transientItemsService: TransientItemsService,
        
    ) {}

    //Loading is for when the page is currently fetching data.  Processing is for when a customer makes an action:
    loading = false;
    processing = false;
    @Input() personBankAccount = Mapping();
    personID?;
    decrypted = false;
    IBVerificationStatus = IBVerificationStatus;
    
    form = this.formBuilder.group({
        routing: ['', Validators.required],
        primary: [''],
        account: ['', Validators.required],
        type: ['', Validators.required],
        name: ['', Validators.required]
    });

    async ngOnInit() {
        this.form.valueChanges
            .pipe(filter(_ => this.form.valid))
            .subscribe(() =>
                this.transientItemsService.subject.next({
                    personBankAccount: this.personBankAccount
                })
            );
        
        this.route.paramMap.pipe(
            filter(paramMap => paramMap.has('customerID')),
            map(paramMap => ({
                CustomerID: Number(paramMap.get('customerID')),
                ID: paramMap.get('id')
            }))
        ).subscribe(async ({ CustomerID, ID }) => {
            this.personID = CustomerID;
            if (ID !== null)
                this.personBankAccount = await this.models.byID('PersonBankAccount', Number(ID), Mapping());
            this.loading = false;
        });
        
        if (this.route.snapshot.data.personBankAccount) {
            this.personBankAccount = this.route.snapshot.data.personDebitCard;
        }
    }

    async fetchBank() {
        if (
            !this.personBankAccount ||
            !this.personBankAccount.BankAccount ||
            !this.personBankAccount.BankAccount.Bank
        )
            return;

        this.processing = true;
        const request = {
            Number: this.personBankAccount.BankAccount.Bank.Number
        };
        const mapping = { City: { State: {} } };
        try {
            const bank = await this.models.select('Bank', request, mapping);
            if (bank) this.personBankAccount.BankAccount.Bank.Name = bank.Name;
        } finally {
            this.processing = false;
        }
    }
    redirect() {
        this.location.back();
    }
    async Submit() {
        if (!this.form.valid) {
            markAllControlsAsTouched(this.form);
            return;
        }

        this.processing = true;

        if (this.personBankAccount && this.personBankAccount.Person)
            this.personBankAccount.Person.ID = this.personID;

        try {
            await this.models.save('PersonBankAccount', this.personBankAccount);
        } finally {
            this.processing = false;
            this.redirect();
        }
    }

    async decrypt(bankAccount: { ID: number; Number?: string }) {
        bankAccount.Number = await this.models.decryptProperty(
            'BankAccount',
            bankAccount.ID,
            'Number'
        );
        this.decrypted = true;
    }

    async Delete() {
        this.processing = true;
        if (this.personBankAccount.ID)
            await this.models.delete('PersonBankAccount', this.personBankAccount.ID);
        this.redirect();
        this.processing = false;
    }
}
function Mapping(): PersonBankAccount {
    return {
        BankAccount: { Bank: <Bank>{}, Type: {} },
        Person: <Person>{}
    };
}
