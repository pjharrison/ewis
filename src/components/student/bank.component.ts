import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as EWIS from '../../../EWIS/_import';
import { IBVerificationStatus } from '../../../MLC/Types/IBVerificationStatus';
import { Global } from '../../services/global';

import { flatMap, map, filter } from 'rxjs/operators';
import BankService from '../../services/bank.service';

@Component({
    selector: 'bank',
    templateUrl: './bank.component.html',
    styleUrls: ['./bank.component.css']
})
export default class BankComponent {
    constructor(readonly global: Global, readonly route: ActivatedRoute, readonly router: Router, , readonly bankService: BankService) { }

    //Loading is for when the page is currently fetching data.  Processing is for when a customer makes an action:
    loading = false;
    decrypted = false;
    ibv?: EWIS.Types.IBVerification;
    IBVerificationStatus = IBVerificationStatus;
    person: EWIS.Types.Person = Mapping();

    async ngOnInit() {
        this.loading = true;

        this.route.paramMap.pipe(
            filter(paramMap => paramMap.has('customerID')),
            map(paramMap => Number(paramMap.get('customerID'))),
            flatMap(async customerID => {
                this.loading = true;
                try {
                    return await this.models.byID('Person', customerID, Mapping());
                } finally {
                    this.loading = false;
                }
            })
        ).subscribe(async person => {
            this.person = person;
            await this.bankService.checkPersonIBV(person.ID);
            this.loading = false;
        });        
    }

    async sendIBV(PersonBank: EWIS.Types.PersonBankAccount) {
        if (PersonBank) {
            const response = await this.bankService.initiateIBV({ PersonBankAccount: PersonBank, redirectRoute: 'bank' });
            window.open(response.LoginUrl);
        }
    }
    async viewReport() {
        if (this.ibv)
            window.open(this.ibv.ReportUrl);
    }

    async decrypt(bankAccount: { ID: number, Number?: string }) {
        bankAccount.Number = await this.models.decryptProperty('BankAccount', bankAccount.ID, 'Number');
        this.decrypted = true;
    }
}
function Mapping() {
    return <EWIS.Types.Person>{
        PrimaryBankAccount: {
            BankAccount: {
                Bank: {},
                Type: {}
            },
            IBVerifications: [{}]
        },
        BankAccounts: [{ IBVerifications: [{}], BankAccount: { Bank: {}, Type: {} } }]
    };
}