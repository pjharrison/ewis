import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import ValidationService from '../../shared/services/validation.service';

import CustomerService from '../../services/customer.service';
import * as EWIS from '../../../EWIS/_import';
import { Global } from '../../services/global';
import UserService from '../../services/user.service';
import markAllControlsAsTouched from '../../utilities/mark-all-controls-as-touched';

@Component({
    selector: 'createcustomer',
    templateUrl: './create.component.html'
})
export default class CreateComponent {
    constructor(readonly global: Global, readonly router: Router, readonly formBuilder: FormBuilder, readonly validationService: ValidationService, readonly location: Location, , readonly userService: UserService, readonly customerService: CustomerService) {}

    customer: EWIS.Types.Customer | undefined;
    processing = false;
    loading = false;
    checkSsn = async (control: AbstractControl) => (await this.userService.canRegisterCustomer(control.value)) ? null : { alreadyRegistered: true }; // tslint:disable-line:no-null-keyword
    form = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        ssn: new FormControl('', {
            validators: [Validators.required, Validators.minLength(9)],
            asyncValidators: this.checkSsn,
            updateOn: 'blur'
        }),
        dob: ['', [Validators.required, this.validationService.dob]],
        address: ['', Validators.required],
        subpremise: [''],
        zip: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required]
    });

    dobPickerConfiguration = {
        maxDate: {
            year: new Date().getFullYear() - 18,
            month: new Date().getMonth(),
            day: new Date().getDate()
        }
    };

    async Submit() {
        if (!this.form.valid) {
            markAllControlsAsTouched(this.form);
            return;
        }
        this.processing = true;
        try {
            const { state, zip, city, address, subpremise, firstName, lastName, dob, ssn } = this.form.value;
            this.customer = await this.customerService.save({
                Person: {
                    FirstName: firstName,
                    LastName: lastName,
                    DOB: dob,
                    SSN: ssn,
                    PrimaryAddress: {
                        Address: {
                            Street: address,
                            Subpremise: subpremise,
                            Zip: {
                                Code: zip,
                                City: {
                                    Name: city,
                                    State: {
                                        Code: state
                                    }
                                }
                            }
                        }
                    }
                }
            });

            this.router.navigate(['/account/' + this.customer.ID]);
        } finally {
            this.processing = false;
        }
    }
    async fetchZip() {
        if (!this.form.value.zip) return;
        this.loading = true;
        const request = { Code: this.form.value.zip };
        const mapping = { City: { State: {} } };
        try {
            const result = await this.models.select('Zip', request, mapping);
            if (result.City.State) {
                this.form.controls.city.setValue(result.City.Name);
                this.form.controls.state.setValue(result.City.State.Code);
            }
        } finally {
            this.loading = false;
        }
    }
}
