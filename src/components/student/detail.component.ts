import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import ValidationService from '../../shared/services/validation.service';
import * as EWIS from '../../../EWIS/_import';

import { Global } from '../../services/global';
import markAllControlsAsTouched from '../../utilities/mark-all-controls-as-touched';
import { flatMap, map, filter } from 'rxjs/operators';
import CustomerStatus from '../../../MLC/Types/CustomerStatus';

@Component({
    selector: 'detail',
    templateUrl: './detail.component.html'
})
export default class DetailComponent {
    constructor(readonly global: Global, readonly route: ActivatedRoute, readonly router: Router, readonly formBuilder: FormBuilder, readonly validationService: ValidationService, readonly location: Location, ) { }
    form = this.formBuilder.group({
        status: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        ssn: new FormControl('', { validators: [Validators.required, Validators.minLength(9)] }),
        dob: ['', [Validators.required, this.validationService.dob]],
        address: ['', Validators.required],
        subpremise: [''],
        zip: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        email: ['', Validators.required],
        language: ['', Validators.required],
        optinEmail: [''],
        optinSMS: [''],
        optinMail: ['']
    });
    statuses: Array<CustomerStatus> = [];

    /**
     * `loading` is for when the page is currently fetching `processing` is for when a customer makes an action:
    */
    loading = false;
    processing = false;
    person: EWIS.Types.Person = Mapping();
    emailOn = true;
    __language?: EWIS.Types.CustomerOption;
    get language(): EWIS.Types.CustomerOption {
        if (!this.__language) {
            this.__language = this.person && this.person.Customer && this.person.Customer.Options && this.person.Customer.Options.filter(o => o.Type && typeof o.Type !== 'string' && o.Type.Name === 'Language')[0];
            if (!this.__language)
                this.__language = {
                    Type: { Name: "Language" },
                    Value: "ENGLISH",
                    Customer: this.person.Customer
                };
        }
        return this.__language;
    }

    async ngOnInit() {
        this.loading = true;
        if (!this.global.hasPermission('RVP')) {
            this.form.controls.firstName.disable();
            this.form.controls.lastName.disable();
            this.form.controls.status.disable();
            this.form.controls.ssn.disable();
        }
        this.route.paramMap.pipe(
            filter(paramMap => paramMap.has('customerID')),
            map(paramMap => Number(paramMap.get('customerID'))),
            flatMap(async customerID => {
                this.loading = true;
                try {
                    return await this.models.byID('Person', customerID, Mapping());
                } finally {
                    this.loading = false;
                }
            })
        ).subscribe(async person => {
            if (!person.PrimaryAddress)
                person.PrimaryAddress = { Address: { Zip: { City: { State: {} } } } };
            if (!person.Emails || !person.Emails.length) {
                person.Emails = [{ Email: { Address: "N/A" }, PersonID: person.ID }];
                this.emailOn = false;
                this.form.controls.email.setValue("N/A");
            }

            this.form.controls.dob.setValue(person.DOB ? new Date(person.DOB) : new Date());
            if (person.Customer && person.Customer.Options) {
                const optinEmail = person.Customer.Options.filter(o => o => o.Type && typeof o.Type !== 'string' && o.Type.Name === "OptIn-Email")[0];
                if (optinEmail && optinEmail.Value === "true")
                    this.form.controls.optinEmail.setValue(true);
                else
                    this.form.controls.optinEmail.setValue(false);

                const optinSMS = person.Customer.Options.filter(o => o.Type && typeof o.Type !== 'string' && o.Type.Name === "OptIn-SMS")[0];
                if (optinSMS && optinSMS.Value === "true")
                    this.form.controls.optinSMS.setValue(true);
                else
                    this.form.controls.optinSMS.setValue(false);

                const optinMail = person.Customer.Options.filter(o => o.Type && typeof o.Type !== 'string' && o.Type.Name === "OptIn-Mail")[0];
                if (optinMail && optinMail.Value === "true")
                    this.form.controls.optinMail.setValue(true);
                else
                    this.form.controls.optinEmail.setValue(false);
            }

            this.person = person;
        });
    }
    dobPickerConfiguration = {
        maxDate: {
            year: new Date().getFullYear() - 18,
            month: new Date().getMonth(),
            day: new Date().getDate()
        }
    };
    toggleEmail() {
        if (this.emailOn) {
            this.emailOn = false;
            this.form.controls.email.setValue("N/A");
            if (this.person.Emails && this.person.Emails[0] && this.person.Emails[0].Email)
                this.person.Emails[0] = {
                    Email: {
                        Address: 'N/A'
                    }
                };
        } else {
            this.emailOn = true;
        }
    }
    async Decrypt() {
        if (!this.person.Customer || !this.person.Customer.ID)
            return;
        this.person.SSN = await this.models.decryptProperty("Customer", this.person.Customer.ID, "SSN");
    }
    async fetchZip() {
        if (!this.person.PrimaryAddress || !this.person.PrimaryAddress.Address || !this.person.PrimaryAddress.Address.Zip) return;
        this.loading = true;
        const request = { Code: this.person.PrimaryAddress.Address.Zip.Code };
        const mapping = { City: { State: {} } };
        try {
            const result = await this.models.select('Zip', request, mapping);
            if (this.person && this.person.PrimaryAddress && this.person.PrimaryAddress.Address && this.person.PrimaryAddress.Address.Zip && this.person.PrimaryAddress.Address.Zip.City && this.person.PrimaryAddress.Address.Zip.City.State) {
                this.person.PrimaryAddress.Address.Zip.City.Name = result.City.Name;
                this.person.PrimaryAddress.Address.Zip.City.State.Code = result.City.State && result.City.State.Code;
            }
        } finally {
            this.loading = false;
        }
    }
    async Submit() {
        if (!this.form.valid) {
            markAllControlsAsTouched(this.form);
            return;
        }

        try {
            var storeID = this.global.storeID;
            const { state } = this.form.value;
            if (state === "MO") {
                storeID = 84;
            }
            this.processing = true;

            this.person.DOB = new Date(this.form.value.dob);
            if (this.person.Customer) {
                delete this.person.Customer.Options;
                delete this.person.Customer.Person;
            }
            const tempPerson = {
                ...this.person,
                Customer: {
                    ...this.person.Customer,
                    Store: { ID: storeID },
                    Options: [{ Type: { Name: 'Language' }, Value: this.form.controls.language.value }, { Type: { Name: 'OptIn-Email' }, Value: this.form.controls.optinEmail.value ? 'true' : 'false' }, { Type: { Name: 'OptIn-Mail' }, Value: this.form.controls.optinMail.value ? 'true' : 'false' }, { Type: { Name: 'OptIn-SMS' }, Value: this.form.controls.optinSMS.value ? 'true' : 'false' }]
                }
            };
            this.global.Person = await this.models.save('Person', tempPerson, {
                Customer: {
                    Store: {
                        Address: { Zip: { City: { State: {} } } },
                        Brand: {},
                        MainPhone: { Phone: {} },
                        Email: {}
                    }
                }
            });
        } catch {
            this.router.navigate(['/error']);
        } finally {
            this.processing = false;
        }
    }
}

function Mapping() {
    return <EWIS.Types.Person>{
        Customer: {
            Options: [{ Type: {} }],
            Status: { Type: {} },
            Store: {
                Address: {
                    Zip: { City: { State: {} } }
                }
            }
        },
        PrimaryAddress: {
            Address: {
                Zip: { City: { State: {} } }
            }
        },
        PrimaryPhone: {
            Phone: {}
        },
        Emails: [
            {
                Email: {}
            }
        ],
        Phones: [
            {
                Phone: {}
            }
        ]
    };
}
