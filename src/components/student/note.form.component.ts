import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import * as EWIS from '../../../EWIS/_import';
import { Global } from '../../services/global';


@Component({
    selector: 'noteform',
    templateUrl: './note.form.component.html',
    styleUrls: ['./note.form.component.css']
})
export default class NoteFormComponent {
    constructor(readonly global: Global, readonly router: Router, ) { }

    @Input() customer?: EWIS.Types.Customer;
    @Output() success = new EventEmitter();
    activeLoans?: Array<EWIS.Types.Loan>;
    loading: boolean = false;
    processing: boolean = false;
    note: EWIS.Types.Note = Mapping();
    isPTP: boolean = false;
    date?: Date;
    
    async ngOnInit() {
        if(!this.customer || !this.customer.ID)
            return;
        this.loading = true;
        this.activeLoans = await this.models.getProperty("Customer", this.customer.ID, "ActiveLoans", { Status: { Type: {} }, Balances: {}});
        this.loading = false;
    }

    async Submit() {
        this.processing = true;
        this.note.User = { ID: this.global.userID };
        this.note.Customer = { ID: this.customer &&  this.customer.ID };
        if (this.note.LoanRequest && !this.note.LoanRequest.ID)
            delete this.note.LoanRequest;
        this.note = await this.models.save("CustomerNote", this.note, { LoanRequest: {}, User: {} });
        this.success.emit(this.note);
        this.note = Mapping();
        this.isPTP = false;
        this.processing = false;
    }
    async Reset() {
        this.processing = true;
        this.note = Mapping();
        this.isPTP = false;
        this.processing = false;
    }
}
function Mapping(): EWIS.Types.CustomerNote {
    return { User: {}, LoanRequest: {}, Customer: {}, Date: undefined, Message: undefined, PTPDate: undefined };
}