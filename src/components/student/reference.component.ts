import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { flatMap, map, filter } from 'rxjs/operators';

import * as EWIS from '../../../EWIS/_import';

@Component({
    selector: 'reference',
    templateUrl: './reference.component.html'
})
export default class ReferenceComponent {
    constructor(readonly route: ActivatedRoute, ) { }

    loading: boolean = false;
    processing: boolean = false;
    person: EWIS.Types.Person = Mapping();
    @Input() customer: EWIS.Types.Customer | undefined;

    async ngOnInit() {
        this.loading = true;
        this.route.paramMap.pipe(
            filter(paramMap => paramMap.has('customerID')),
            map(paramMap => Number(paramMap.get('customerID'))),
            flatMap(async customerID => {
                try {
                    return await this.models.byID('Person', customerID, Mapping());
                } finally {
                    this.loading = false;
                }
            })
        ).subscribe(async person => {
            this.person = person
        });
    }
}
function Mapping() {
    return {
        Customer: <EWIS.Types.Person>{
            References: [{
                Type: {},
                Person: {
                    Phones: [{
                        Phone: {
                            Type: {}
                        },
                        Status: {},
                        Person: {}
                    }]
                }
            }]
        }
    };
}