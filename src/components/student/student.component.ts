import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { map, filter, flatMap } from 'rxjs/operators';
import { Global } from '../../services/global';
import * as EWIS from '../../../EWIS/_import';


@Component({
    selector: 'student',
    templateUrl: './student.component.html',
    styleUrls: ['./student.component.css']
})
export default class StudentComponent {
    constructor(
        readonly router: Router,
        readonly route: ActivatedRoute,
        readonly modalService: NgbModal,
        readonly global: Global
    ) { }

    issueType = EWIS.Types.IssueType;
    issueStatus = EWIS.Types.IssueStatus;
    @ViewChild('PMLModal') pmlModal;
    @ViewChild('Issues') issueModal;
    processing = false;
    loading = false;
    hasIssues = false;
    modalRef?: NgbModalRef;
    password?: string;
    invalidPassword?: string;
    application?: EWIS.Types.Application;
    student?: Customer;
    activity: any = {};

    get totalNMI(): number {
        if (this.student && this.student.Person && this.student.Person.Income)
            return this.student.Person.Income.reduce((a, b) => +a + +(b.NMI || 0), 0);
        return 0;
    }

    async ngOnInit() {
        this.issueService.issues$.subscribe(issues => { this.hasIssues = issues.length > 0; });

        this.route.paramMap.pipe(
            filter(paramMap => paramMap.has('customerID')),
            map(paramMap => Number(paramMap.get('customerID'))),
            flatMap(async customerID => {
                this.loading = true;
                try {
                    return await this.models.byID('Customer', customerID, Mapping());
                } finally {
                    this.loading = false;
                }
            })
        ).subscribe(async customer => {
            this.student = customer;
            if (!this.student) {
                this.router.navigate(["/home"]);
                return;
            }

            this.issueService.sync(customer.ID);
            if (localStorage.application) {
                this.application = JSON.parse(localStorage.application);
                if (this.application && this.application.Customer && this.application.Customer.ID === this.student.ID)
                    this.router.navigate(["step"], { relativeTo: this.route });
            }

            this.activity = await this.models.procedure("ListLoanRequestsOrLoansByCustomer", [{ Name: "CustomerID", Value: this.student.ID }, { Name: "Type", Value: "Pending And Active" }]);
        });
    }

    removeApplication() {
        delete localStorage.application;
        delete localStorage.step;
        delete this.application;
        this.loanService.setPreviousLoan(undefined);
        this.ngOnInit();
    }
    async clarityUpdate() {
        if (!this.password || !this.global.User)
            return;

        this.processing = true;
        try {
            // TODO: review
            await this.userService.login({ Username: this.global.User.Username, Password: this.password });
            if (this.student) {
                this.student.ScoreCard = await this.customerService.fetchScoreCard(this.student.ID);
                this.modalRef && this.modalRef.close();
            }
        } catch (e) {
            this.invalidPassword = "The password you entered is invalid.  Please try again.";
        }

        this.processing = false;
    }
    open(template) {
        this.modalRef = this.modalService.open(template);
    }
    openPML() {
        this.modalService.open(this.pmlModal);
    }
    openIssues() {
        this.modalRef = this.modalService.open(this.issueModal);
    }
    closeIssues(issue: { Module: any; }) {
        if (this.modalRef)
            this.modalRef.close();
        this.router.navigate([issue.Module], { relativeTo: this.route });
    }
    get pmls() {
        return (
            this.student &&
            this.student.ScoreCard &&
            this.student.ScoreCard.PotentialMaxLoan &&
            Object.entries(this.student.ScoreCard.PotentialMaxLoan).map(([Name, Value]) => ({ Name, Value }))
        ) || [];
    }
}
function Mapping(): EWIS.Types.Customer {
    return {
        ActiveLoans: [{}],
        Store: {},
        ScoreCard: { PotentialMaxLoan: {} },
        Person: { Income: [{}] }
    };
}
