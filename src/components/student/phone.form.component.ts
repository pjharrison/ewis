import { Component, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import * as EWIS from '../../../EWIS/_import';

import { Global } from '../../services/global';
import PhoneService from '../../services/phone.service';
import markAllControlsAsTouched from '../../utilities/mark-all-controls-as-touched';

@Component({
    selector: 'phoneform',
    templateUrl: './phone.form.component.html'
})
export default class PhoneFormComponent {
    @ViewChild('RequestPIN') requestPIN;

    constructor(
        readonly global: Global,
        readonly route: ActivatedRoute,
        readonly location: Location,
        readonly formBuilder: FormBuilder,
        readonly modalService: NgbModal,
        ,
        readonly phones: PhoneService) { }

    loading: boolean = false;
    processing: boolean = false;
    requestedPIN: boolean = false;
    pin: string = "";
    pinMsg: string = "";
    personPhone: EWIS.Types.PersonPhone = Mapping();
    person: EWIS.Types.Person = {};

    form = this.formBuilder.group({
        status: ['', Validators.required],
        number: ['', Validators.required],
        extension: [''],
        description: ['', Validators.required]
    });

    async ngOnInit() {
       this.route.paramMap.subscribe(async paramMap => {
            const id = paramMap.get('id');
            this.loading = true;
            try {
                if (id)
                    this.personPhone = await this.models.byID('PersonPhone', Number(id), Mapping());

                if (!this.personPhone.Status)
                    this.personPhone.Status = {};
                const personID = paramMap.get('personID');
                if (personID === null)
                    this.person = this.personPhone && this.personPhone.Person || {};
                else
                    this.person = await this.models.byID('Person', Number(personID));

            } finally {
                this.loading = false;
            }
        });
    }
    redirect() {
        this.location.back();
    }

    async Submit() {
        if (!this.form.valid) {
            markAllControlsAsTouched(this.form);
            return;
        }

        if (!this.person.ID)
            return;

        this.personPhone.Person = { ID: this.person.ID };
        try {
            this.processing = true;
            const result: EWIS.Types.PersonPhone = await this.models.save('PersonPhone', this.personPhone, { Phone: { Type: {} }});
            if (result && result.Phone && result.Phone.Type && result.Phone.Type.Name === "Mobile")
                this.modalService.open(this.requestPIN);
            else
                this.redirect();
        } finally {
            this.processing = false;
        }
    }
    async Delete() {
        this.processing = true;
        if (this.personPhone.ID)
            await this.models.delete("PersonPhone", this.personPhone.ID);
        this.redirect();
        this.processing = false;
    }

    async csrRequestPIN() {
        if (!this.personPhone || !this.personPhone.ID)
            return;
        this.processing = true;
        try {
            await this.phones.requestPIN(this.personPhone.ID);
            this.requestedPIN = true;
        } catch (e) {
            this.pinMsg = e;
        }  finally {
            this.processing = false;
        }
    }

    async ConfirmPIN() {
        if (!this.personPhone || !this.personPhone.ID)
            return;
        this.processing = true;
        try {
            const result = await this.phones.confirmPIN(this.personPhone.ID, this.pin);
            if (result === undefined)
                this.pinMsg = "Invalid PIN.  Please double check the PIN entered.";
            else
                this.redirect();
        } catch (e) {
            this.pinMsg = e;
        } finally {
            this.processing = false;
        }
    }
}
function Mapping() {
    return {
        Person: {},
        Status: {},
        Phone: {
            Type: {}
        }
    };
}