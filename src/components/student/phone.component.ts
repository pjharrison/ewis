import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as EWIS from '../../../EWIS/_import';
import { map, filter } from 'rxjs/operators';

@Component({
    selector: 'phone',
    templateUrl: './phone.component.html'
})
export default class PhoneComponent {
    constructor(readonly route: ActivatedRoute, ) { }

    loading = false;
    processing = false;
    person: EWIS.Types.Person = Mapping();

    ngOnInit() {
        this.route.paramMap.pipe(
            filter(paramMap => paramMap.has('personID')),
            map(paramMap => Number(paramMap.get('personID')))
        ).subscribe(async personID => {
            this.loading = true;
            try {
                this.person = await this.models.byID('Person', personID, Mapping());
            }
            finally {
                this.loading = false;
            }
        });
    }
}
function Mapping(): EWIS.Types.Person {
    return {
        PrimaryPhone: {
            Person: {},
            Status: {},
            Phone: {
                Type: {}
            }
        },
        Phones: [{
            Person: {},
            Status: {},
            Phone: {
                Type: {}
            }
        }]
    };
}