import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import * as EWIS from '../../../EWIS/_import';
import { flatMap, map, filter } from 'rxjs/operators';
import { Global } from '../../services/global';


@Component({
    selector: 'note',
    templateUrl: './note.component.html'
})
export default class NoteComponent {
    constructor(readonly global: Global, readonly route: ActivatedRoute, readonly router: Router, ) { }

    @Input() Customer: EWIS.Types.Customer | undefined;
    loading: boolean = false;
    processing: boolean = false;
    page: number = 1;
    totalPages: number = 1;
    customer: EWIS.Types.Customer = Mapping();

    async ngOnInit() {
        this.loading = true;

        this.route.paramMap.pipe(
            filter(paramMap => paramMap.has('customerID')),
            map(paramMap => Number(paramMap.get('customerID'))),
            flatMap(async customerID => {
                this.loading = true;
                try {
                    return await this.models.byID('Customer', customerID, Mapping());
                } finally {
                    this.loading = false;
                }
            })
        ).subscribe(async customer => {
            this.customer = customer;
        });
    }
    addNote($event: EWIS.Types.CustomerNote) {
        this.customer && this.customer.Notes && this.customer.Notes.push($event);
    }
}
function Mapping(): EWIS.Types.Customer {
    return { Notes: [{ LoanRequest: {}, User: {} }] };
}