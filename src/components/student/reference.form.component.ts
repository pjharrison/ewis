import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import * as EWIS from '../../../EWIS/_import';

import { Global } from '../../services/global';
import { filter, map } from 'rxjs/operators';

@Component({
    selector: 'referenceform',
    templateUrl: './reference.form.component.html'
})
export default class ReferenceFormComponent {
    constructor(
        readonly global: Global,
        readonly route: ActivatedRoute,
        readonly location: Location,
        readonly formBuilder: FormBuilder,
        ) { }

    loading: boolean = false;
    processing: boolean = false;
    reference: EWIS.Types.CustomerReference = Mapping();
    person: EWIS.Types.Person = {};

    form = this.formBuilder.group({
        type: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required]
    });

    async ngOnInit() {
        this.route.snapshot
        this.route.paramMap.pipe(
            filter(paramMap => paramMap.has('id')),
            map(paramMap => Number(paramMap.get('id')))
        ).subscribe(async id => {
            this.loading = true;
            try {
                this.reference = await this.models.byID('CustomerReference', id, Mapping());
                if (!this.reference.Type) this.reference.Type = {};
            } finally {
                this.loading = false;
            }
        })
}
    redirect() {
        this.location.back();
    }

    async Submit() {
        if (!this.form.valid) {
            Object.keys(this.form.controls).forEach(field => {
                const control = this.form.get(field);
                control && control.markAsTouched({ onlySelf: true });
            });
            return;
        }
        if (!this.global.personID) {
            return;
        }

        this.reference.Customer = { ID: this.global.personID };
        try {
            this.processing = true;
            await this.models.save('CustomerReference', this.reference);
        } finally {
            this.processing = false;
        }
        this.redirect();
    }
    async Delete() {
        this.processing = true;
        if (this.reference.ID)
            await this.models.delete("CustomerReference", this.reference.ID)
        this.redirect();
        this.processing = false;
    }
}
function Mapping(): EWIS.Types.CustomerReference {
    return { Person: {}, Type: {}, Customer: {} };
}