import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import ValidationService from '../../shared/services/validation.service';

import { Global } from '../../services/global';
import * as EWIS from '../../../EWIS/_import';
@Component({
    selector: 'phoneitem',
    templateUrl: './phone.item.component.html'
})
export default class PhoneItemComponent {
    constructor(readonly global: Global, readonly router: Router, readonly formBuilder: FormBuilder, readonly validationService: ValidationService) {}

    loading: boolean = false;
    processing: boolean = false;

    @Input() personPhone?: EWIS.Types.PersonPhone;

    ngOnInit() { }
}