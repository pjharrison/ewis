import { Component, Input, Inject, Optional } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import errorModalContentToken from '../../tokens/error-modal-content.token';
@Component({
    selector: 'error-modal',
    templateUrl: './error-modal.component.html',
    styleUrls: ['./error-modal.component.css']
})
export default class ErrorModalComponent {
    constructor(readonly activeModal: NgbActiveModal, @Optional() @Inject(errorModalContentToken) readonly data) { }
    @Input() error: string | undefined = this.data;
    @Input() title = 'An Error Ocurred';
}