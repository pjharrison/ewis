import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Global } from '../../../services/global';

@Component({
    selector: 'error',
    templateUrl: './error.component.html'
})
export default class ErrorComponent {
    constructor(
        readonly global: Global,
        readonly router: Router,
        readonly formBuilder: FormBuilder
    ) {}
}
