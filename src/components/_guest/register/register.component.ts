import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {
    FormBuilder,
    Validators,
    FormControl,
    AbstractControl
} from '@angular/forms';
import ValidationService from '../../../shared/services/validation.service';

import * as EWIS from '../../../../EWIS/_import';

import { Global } from '../../../services/global';
import UserService from '../../../services/user.service';

@Component({
    selector: 'register',
    templateUrl: './register.component.html'
})
export default class RegisterComponent {
    constructor(
        readonly global: Global,
        readonly router: Router,
        readonly formBuilder: FormBuilder,
        readonly validationService: ValidationService,
        readonly userService: UserService,
        
    ) {}

    //Customer: EWIS.Types.Customer = { Person: { User: {} } };
    customer: EWIS.Types.Customer = { Person: {} };
    user: EWIS.Types.User = {};
    cpassword: string = '';
    processing: boolean = false;

    FetchUsername = async control => {
        try {
            const exists =  control.value && await this.userService.fetchUsername(control.value.trim());
            //Open usernameFound modal template instead:
            return exists ? { existingUsername: true } : null;
        } catch {
            //Username not found, continue registration!
            return null;
        }
    };

    passwordsMatch = ({ value: cpassword }: AbstractControl) => {
        if (!this.form) return null;

        const {
            password: { value: password }
        } = this.form.controls;

        return password && password !== cpassword
            ? { passwordsDoNotMatch: true }
            : null;
    };

    checkSsn = async (control: AbstractControl) => (await this.userService.canRegister(control.value)) ? null : { alreadyRegistered: true };
    
    form = this.formBuilder.group({
        username: new FormControl('', {
            validators: [Validators.required, Validators.email],
            asyncValidators: [this.FetchUsername],
            updateOn: 'blur'
        }),
        password: ['', [Validators.required, this.validationService.password]],
        cpassword: new FormControl('', {
            validators: [Validators.required, this.passwordsMatch],
            updateOn: 'blur'
        }),
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        ssn: new FormControl('', {
            validators: Validators.required,
            asyncValidators: this.checkSsn,
            updateOn: 'blur'
        }),
        dob: new FormControl('', {
            validators: Validators.required,
            updateOn: 'blur'
        })
    });

    dobPickerConfiguration = {
        maxDate: {
            year: new Date().getFullYear() - 18,
            month: new Date().getMonth(),
            day: new Date().getDate()
        }
    };

    async Submit(onSuccess = 'loan') {
        if (!this.form.valid) {
            Object.keys(this.form.controls).forEach(field => {
                const control = this.form.get(field);
                control && control.markAsTouched({ onlySelf: true });
            });
            return;
        }
        if (!this.user.Username)
            return;

        this.processing = true;
        this.customer = await this.models.save('Customer', this.customer);
        if (this.customer && this.customer.Person) {
            this.customer.Person.User = this.user;
            await this.models.save(
                'Person',
                {
                    ...this.customer.Person,
                    Email: { Email: { Address: this.user.Username.trim() }
                }
            });
        }

        await this.userService.login({ Password: this.cpassword, Username: this.user.Username });
        this.processing = false;
        this.router.navigate([onSuccess]);
    }
}
