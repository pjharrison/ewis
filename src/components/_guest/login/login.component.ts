import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import * as EWIS from '../../../../EWIS';
import { Global } from '../../../services/global';
import markAllControlsAsTouched from '../../../utilities/mark-all-controls-as-touched';
import UserService from '../../../services/user.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export default class LoginComponent {
    constructor(readonly global: Global, readonly router: Router, readonly formBuilder: FormBuilder, readonly userService: UserService) {}

    step = 1;
    username = '';
    password = '';
    message = '';
    user: EWIS.Types.User = {};
    loading = false;
    processing = false;

    step1Form = this.formBuilder.group({
        username: ['', Validators.required]
    });
    step2Form = this.formBuilder.group({
        username: [''], // FormGroup blows up without this for some reason. Its value is unused.
        password: ['', Validators.required]
    });

    async ngOnInit() {
        if (this.global.User) {
            this.router.navigate(['/home'], { queryParams: { returnUrl: this.router.url } });
            return;
        }
    }

    async FetchUsername() {
        if (!this.step1Form.valid) return;

        this.processing = true;
        try {
            this.user = await this.userService.fetchUsername(this.username);

            if (this.user) this.step = 2;
            this.processing = false;
            setTimeout(() => {
                const passwordElement = document.getElementById('password');
                if (passwordElement) passwordElement.focus();
            }, 0);
        } catch (e) {
            this.message = e;
        } finally {
            this.loading = false;
            this.processing = false;
        }
    }
    async Login(onSuccess = 'home') {
        if (!this.step2Form.valid) {
            markAllControlsAsTouched(this.step2Form);
            return;
        }

        this.processing = true;
        try {
            console.warn(this.router.url);
            await this.userService.login({ Username: this.username, Password: this.password });
            this.router.navigate([onSuccess]);
        } catch (e) {
            this.step2Form.controls.password.setErrors({
                invalidUsernameOrPassword: true
            });
        } finally {
            this.processing = false;
        }
    }
}
