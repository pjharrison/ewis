import { Component, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import ValidationService from '../../../shared/services/validation.service';

@Component({
    selector: 'forgot',
    templateUrl: './forgot.component.html',
    styleUrls: ['./forgot.component.css']
})
export default class ForgotComponent {
    constructor(readonly router: Router, readonly activeRoute: ActivatedRoute, readonly formBuilder: FormBuilder, readonly validationService: ValidationService) {
        this.activeRoute.params.subscribe((params: Params) => {
            this.type = params["Type"];
        });
    }

    public type: string = "";
    emailForm = this.formBuilder.group({
        dob: ['', Validators.required],
        lastName: ['', Validators.required],
        ssn: ['', Validators.required]
    });
    passwordForm = this.formBuilder.group({
        phone: ['', Validators.required]
    });
}