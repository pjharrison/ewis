import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import * as EWIS from '../../../EWIS/_import';

import { Global } from '../../services/global';

@Component({
    selector: 'print',
    templateUrl: './print.component.html',
    styleUrls: ['./print.component.css']
})
export default class PrintComponent {
    constructor(
        readonly router: Router,
        readonly route: ActivatedRoute,
        readonly global: Global,
        ) { }

    loading = false;
    type = "";
    accruedInterest: Array<EWIS.Types.LoanInterest> = [];
    totalInterestAccrued = 0;
    amortizationSchedule: Array<{}> = [];
    denialCategories?: Array<EWIS.Types.DenialCategory>;
    totalNMI = 0;

    loan?: EWIS.Types.Loan;
    transaction?: EWIS.Types.LoanTransaction;
    vaultTransaction?: EWIS.Types.StoreVaultTransaction;
    check?: EWIS.Types.Check;
    loanRequest?: EWIS.Types.LoanRequest;
    person?: EWIS.Types.Person;
    currentdate = new Date();

    async ngOnInit() {
        this.loading = true;
        this.type = this.route.snapshot.params.type && this.route.snapshot.params.type.toLowerCase();
        const id =  Number(this.route.snapshot.params.id);
        if (!this.type || !id) {
            this.router.navigate(["home"]);
            return;
        }
        switch (this.type) {
            case "loanreceipt":
                this.transaction = await this.models.byID("LoanTransaction", id, { Store: { Phones: [{}], Address: { Zip: { City: { State: {} } } }, Brand: {} }, Loan: { Type: {}, Method: { Type: {} } } });
                console.log(this.transaction);
                break;
            case "bankdepositreceipt":
                this.vaultTransaction = await this.models.byID("StoreVaultTransaction", id, { Item: {} });
                break;
            case "pettycashreceipt":
                this.vaultTransaction = await this.models.byID("StoreVaultTransaction", id, { Item: {} });
                break;
            case "adverseaction":
                this.loanRequest = await this.models.byID("LoanRequest", id, { Status: { Type: {} }, AdverseActions: {}, Store: { Brand: {}, Address: { Zip: { City: { State: {} } } } }, Customer: { Person: { PrimaryAddress: { Address: { Zip: { City: { State: {} } } } } } } });
                //this.denialCategories = await this.models.search("DenialCategory");
                break;
            case "check":
                this.check = await this.models.byID("Check", id, { LoanRequest: { Customer: { Person: { PrimaryAddress: { Address: { Zip: { City: { State: {} } } } } } } } });
                break;
            case "amortizationschedule":
                this.loan = await this.models.byID("Loan", id, { Request: { LoanOffer: { Payments: {} } } });
                this.setAmortizationSchedule();
                break;
            case "paymentschedule":
                this.loan = await this.models.byID("Loan", id, { PaymentSchedule: [{}] });
                break;
            case "personalhistoryforme":
            case "personalhistoryforms":
                this.person = await this.models.byID("Person", id, {
                    PrimaryAddress: { Address: { Zip: { City: { State: {} } } } }, PrimaryBankAccount: {
                        BankAccount: { Bank: {}, Type: {} }
                    },
                    PrimaryDebitCard: {
                        DebitCard: {}
                    },
                    PrimaryPhone: {
                        Phone: {}
                    },
                    Email: {
                        Email: {}
                    },
                    Emails: [
                        { Email: {} }
                    ],
                    PrimaryIncome: { Employment: { Type: {} }, Type: {} },
                    Income: [{ Employment: { Type: {} }, Type: {} }]
                });
                if (this.person && this.person.Income) {
                    this.totalNMI = this.person.Income.reduce((totalNMI, income) => totalNMI + Number(income.Net), 0);
                }
                break;
            case "pastdue":
            case "pastduesecond":
            case "pastduefinal":
                this.loan = await this.models.byID("Loan", id, { Balances: {}, PastDue: { Amount: {}, DueDate: {} }, Customer: { Person: { FirstName: {}, LastName: {}, PrimaryAddress: { Address: { Zip: { City: { State: {} } } } } } }, Request: { Store: { Brand: { Name: {} }, Phones: [{ Phone: {} }], Address: { Zip: { City: { State: {} } } } } } });
                break;
            case "accruedinterest":
                this.loan = await this.models.byID("Loan", id, { LoanInterests: [{}] });
                this.setInterest();
                break;
            default:
                this.loan = await this.models.byID("Loan", id);
                break;
        }
        this.loading = false;
    }
    setInterest() {
        if (this.loan && this.loan.LoanInterests) {
            this.accruedInterest = this.loan.LoanInterests.sort((a, b) => (a && a.Date || 0) > (b && b.Date || 0) ? -1 : 1);
            this.totalInterestAccrued = this.accruedInterest.reduce((a, b) =>  a + Number(b.Amount), 0);
        }
    }
    setAmortizationSchedule() {
        if (this.loan && this.loan.Request && this.loan.Request.LoanOffer && this.loan.Request.LoanOffer.Payments) {
            let balance = this.loan.Request.LoanOffer.Total || 0;

            for (let i = 0; i < this.loan.Request.LoanOffer.Payments.length; i++) {
                const amort = this.loan.Request.LoanOffer.Payments[i];
                balance -= amort.Amount;

                const item = { Number: i, Date: amort.Date, Amount: amort.Amount, Principal: amort.Principal, Interest: amort.Interest, Balance: balance };
                this.amortizationSchedule.push(item);
            }
        }
    }

    print() {
        window.print();
    }
}