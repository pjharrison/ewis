﻿import {
    Router,
    CanActivate,
    CanActivateChild
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Global } from '../services/global';

@Injectable({ providedIn: 'root' })
export default class LoggedInGuard implements CanActivate, CanActivateChild {
    constructor(readonly router: Router, readonly global: Global) {}

    async canActivate() {
        if (!this.global.User || !this.global.Store || !this.global.Workstation) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }

    canActivateChild() {
        return this.canActivate();
    }
}
