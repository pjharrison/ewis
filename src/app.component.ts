
import { Component } from '@angular/core';
import {
    NgbDateNativeAdapter,
    NgbDateAdapter
} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export default class AppComponent {
    constructor() { }
}
