import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';

import SharedModule from './shared/shared.module';
import AppComponent from './app.component';

/////////////// GUEST \\\\\\\\\\\\\\\
import LoginComponent from './components/_guest/login/login.component';
import ForgotComponent from './components/_guest/forgot/forgot.component';

///////////////  All  \\\\\\\\\\\\\\\
import NavComponent from './components/nav/nav.component';
import SearchComponent from './components/account/search/search.component';
import ErrorComponent from './components/_guest/error/error.component';

/////////////// ACCOUNT \\\\\\\\\\\\\\\
import HomeComponent from './components/account/home.component';
import HomeMessagesComponent from './components/account/home.messages.component';
import HomeStatsComponent from './components/account/home.stats.component';
import QueueMessagesComponent from './components/account/queue.message.component';
import QueueMessageListComponent from './components/account/queue.message.list.component';
import QueueMessageItemComponent from './components/account/queue.message.item.component';
import QueueMessageComposeComponent from './components/account/queue.message.compose.component';
import PrintComponent from './components/print/print.component';


/////////////// STUDENT \\\\\\\\\\\\\\\\\\\\
import AccountComponent from './components/student/account.component';
import DetailComponent from './components/student/detail.component';
import BankComponent from './components/student/bank.component';
import BankFormComponent from './components/student/bank.form.component';
import CreateComponent from './components/student/create.component';
import ReferenceComponent from './components/student/reference.component';
import ReferenceFormComponent from './components/student/reference.form.component';
import PhoneComponent from './components/student/phone.component';
import PhoneItemComponent from './components/student/phone.item.component';
import PhoneFormComponent from './components/student/phone.form.component';
import NoteComponent from './components/student/note.component';
import NoteFormComponent from './components/student/note.form.component';


/////////////// Guards \\\\\\\\\\\\\\\
import LoggedInGuard from './guards/logged-in.guard';

/////////////// Services \\\\\\\\\\\\\\\
import MomentjsNgbDateparserFormatter from './services/momentjs-ngb-dateparser-formatter.service';
import ErrorModalComponent from './components/error-modal/error-modal.component';

const canActivate = [LoggedInGuard];
const canActivateChild = [LoggedInGuard];

@NgModule({
    imports: [
        HttpClientModule,
        ChartsModule,
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        BrowserAnimationsModule,
        NgbModule.forRoot(),
        RouterModule.forRoot(
            [
                /////////////// GUEST \\\\\\\\\\\\\\\
                { path: 'login', component: LoginComponent },
                { path: 'forgot/:Type', component: ForgotComponent },

                ///////////////  All  \\\\\\\\\\\\\\\
                { path: 'error', component: ErrorComponent },

                /////////////// CSR \\\\\\\\\\\\\\\
                { path: 'home', component: HomeComponent, canActivate },
                { path: 'print/:type/:id', component: PrintComponent, canActivate },
                { path: 'queuemessage', component: QueueMessagesComponent, canActivate },
                { path: 'queuemessageitem', component: QueueMessageItemComponent, canActivate },
                { path: 'queuemessagecompose', component: QueueMessageComposeComponent, canActivate },
                
                /////////////// STUDENT \\\\\\\\\\\\\\\
                { path: 'create', component: CreateComponent, canActivate: canActivate },
                {
                    path: 'student/:studentID',
                    component: AccountComponent,
                    canActivate,
                    children: [
                        { path: '', redirectTo: 'detail', pathMatch: 'full' },
                        { path: 'detail', component: DetailComponent },
                        { path: 'bank', component: BankComponent },
                        { path: 'bank/bankform', component: BankFormComponent },
                        { path: 'bank/bankform/:id', component: BankFormComponent },
                        { path: 'reference', component: ReferenceComponent },
                        { path: 'reference/referenceform', component: ReferenceFormComponent },
                        { path: 'reference/referenceform/:id', component: ReferenceFormComponent },
                        { path: 'reference/phoneform', component: PhoneFormComponent },
                        { path: 'reference/phoneform/:id', component: PhoneFormComponent },
                        { path: 'phone/:personID', component: PhoneComponent },
                        { path: 'phone/:personID/phoneform', component: PhoneFormComponent },
                        { path: 'phone/:personID/phoneform/:id', component: PhoneFormComponent },
                        { path: 'note', component: NoteComponent }
                    ]
                },
                { path: '', pathMatch: 'full', redirectTo: '/login' }
            ],
            {
                paramsInheritanceStrategy: "always",
                relativeLinkResolution: "corrected",
                errorHandler: error => {
                    console.error('Could not navigate to the requested route: \n', error);
                },
                //enableTracing: true, //!env.prod,
                useHash: true //!env.prod
            }
        ),
        SharedModule
    ],
    bootstrap: [AppComponent],
    entryComponents: [AppComponent, LoginComponent, ErrorModalComponent],
    providers: [
        { provide: NgbDateParserFormatter, useClass: MomentjsNgbDateparserFormatter }
    ],
    declarations: [
        AppComponent,
        NavComponent,

        /////GUEST\\\\\
        ErrorComponent,
        LoginComponent,
        ForgotComponent,

        /////ALL\\\\\
        HomeComponent,
        HomeMessagesComponent,
        HomeStatsComponent,
        SearchComponent,
        QueueMessagesComponent,
        QueueMessageListComponent,
        QueueMessageComposeComponent,
        QueueMessageItemComponent,
        PrintComponent,
        ErrorModalComponent,

        /////STUDENT\\\\\
        AccountComponent,
        BankComponent,
        CreateComponent,
        DetailComponent,
        BankFormComponent,
        ReferenceComponent,
        ReferenceFormComponent,
        PhoneComponent,
        PhoneItemComponent,
        PhoneFormComponent,
        NoteComponent,
        NoteFormComponent        
    ]
})
export default class AppModule {}