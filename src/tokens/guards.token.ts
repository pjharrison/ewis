import { InjectionToken } from "@angular/core";

export default new InjectionToken('guards');