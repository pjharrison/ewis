import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import ErrorModalComponent from '../components/error-modal/error-modal.component';
import env from 'env';

@Injectable({
  providedIn: 'root'
})
export default class DialogService {
    constructor(readonly modal: NgbModal) {}
    async error(error: string | Error | object) {
        const content = typeof error === 'string' ? error : error instanceof Error ? (error.message + (env.verbose ? '\n' + error.stack : '')) : Object.entries(error).map(([key, value]) => `${key}: ${value}`).join('\n');
        return new Promise(resolve =>
            // setTimeout is necessary due to a bug in Angular itself. See
            // https://github.com/angular/angular/issues/15634
            // https://github.com/ng-bootstrap/ng-bootstrap/issues/1384
            setTimeout(() => {
                const modalRef = this.modal.open(ErrorModalComponent);
                modalRef.componentInstance.error = content;
                return modalRef.result.finally(resolve);
            }));
    }
}
