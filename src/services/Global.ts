﻿import { Router } from '@angular/router';

import * as EWIS from '../../EWIS/_import';
import moment from 'moment';
import withExpiry from '../utilities/with-expiry';
import HttpService from './http.service';
import { TypesModel } from '../../EWIS/TypesModel';
import Permission, { normalizeRole, Permissions } from '../../EWIS/Permission';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class Global {
    constructor(readonly router: Router, readonly http: HttpService) {
            router.events.subscribe(() => {
            this.User = this.User;
        });
    }
    logout(redirect = '/login'): void {
        Object.keys(localStorage).forEach(key => {
            switch (key) {
                default:
                    delete localStorage[key];
                    break;
            }
        });

        delete this.__user;
        delete this.__types;
        delete this.__role;

        this.router.navigateByUrl(redirect).catch(console.error);
    }

    //////////////////// Login Variables \\\\\\\\\\\\\\\\\\\\
    private __user?: EWIS.Types.User & { expiry: moment.Moment };
    get User() {
        let json = localStorage.user;
        if (json) this.__user = JSON.parse(json);
        if (this.__user) {
            const now = moment();
            if (now.isAfter(this.__user.expiry)) {
                this.logout();
                return undefined;
            }
        }
        return this.__user;
    }
    set User(value) {
        this.__user = value && withExpiry(value);
        if (this.__user) {
            localStorage.user = JSON.stringify(this.__user);
        } else {
            delete localStorage.user;
            this.__user = undefined;
        }
    }
    
    private __role?: EWIS.Types.Role;
    get Role() {
        if (this.User && this.User.Role) {
            if (!this.__role)
                this.__role = this.User.Role;
        }
        return this.__role;
    }
    set Role(value) {
        this.__role = value;
    }

    ////////// Helper Variables \\\\\\\\\\
    get userID() {
        return this.User && this.User.ID;
    }
    
    private __types: {} | TypesModel | undefined;
    public get Types() {
        if (!this.__types) {
            this.fetchTypes().then(types => {
                this.__types = types;
            });
        }
        return this.__types || {};
    }
    async fetchTypes() {
        if (this.__types)
            return this.__types;
        return await this.http.post(
            '',
            { method: 'Types', controller: 'Model' },
            { Input: {} }
        );
    }
    hasPermission(role: Permission): boolean {
        role = normalizeRole(role);
        switch (this.Role && this.Role.Type && this.Role.Type.Name) {
            case "Administrator":
                return true;
            case "Instructor":
                if (role !== "Administrator")
                    return true;
                return false;
            case "Guardian":
                if (role === "Administrator" || role === "Instructor")
                    return true;
                break;
            case "Student":
                if (role === "Student")
                    return true;
                break;
            default:
                return false;
        }
        return false;
    }
}
