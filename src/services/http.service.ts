import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EndPoint, post, get } from '../http/post';
import DialogService from './dialog.service';

const requesting = new BehaviorSubject(false);

@Injectable({ providedIn: 'root' })
export default class HttpService {
    constructor(readonly dialogs: DialogService) {}
    async post(type: string, endPoint: EndPoint, body: object): Promise<any>;
    async post(endPoint: EndPoint, body: object): Promise<any>;
    async post(typeOrEndPoint: string | EndPoint, endPointOrBody: EndPoint | object, body?: object) {
        requesting.next(true);
        try {
            return await post(
                <any>typeOrEndPoint,
                <any>endPointOrBody,
                <any>body
            );
        } catch (e) {
            this.dialogs.error(e.message || e);
            throw e;
        } finally {
            requesting.next(false);
        }
    }

    async get(endPoint: string | EndPoint) {
        requesting.next(true);
        try {
            return await get(endPoint);
        } finally {
            requesting.next(false);
        }
    }

    async fetch(input: RequestInfo, init?: RequestInit) {
        requesting.next(true);
        try {
            return await fetch(input, init);
        } finally {
            requesting.next(false);
        }
    }

    get requesting() {
        return requesting.asObservable();
    }
}
