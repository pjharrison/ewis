import { Injectable } from '@angular/core';
import HttpService from './http.service';
import withExpiry from '../utilities/with-expiry';

@Injectable({
    providedIn: 'root'
})
export default class UserService {
    constructor(readonly http: HttpService) {}
    async login(credentials: { Username?: string, Password?: string }) {
        const user = await this.http.post(
            'MLC.Data.Model.User',
            { method: 'Signin', controller: 'User' },
            {
                Input: { ...credentials },
                TypeMapping: {
                    Person: {
                        Customer: {
                            Store: {
                                Address: {
                                    Zip: {
                                        City: {
                                            State: {}
                                        }
                                    }
                                },
                                Brand: {},
                                MainPhone: {
                                    Phone: {}
                                },
                                Email: {},
                                Workstations: [{}]
                            }
                        }
                    },
                    Roles: {
                        Type: {},
                        StoreRoles: {
                            Store: {
                                Status: {}
                            },
                            Role: {
                                Type: {}
                            }
                        }
                    }
                }
            }
        );

        const userWithExpiry = withExpiry(user);
        localStorage.user = JSON.stringify(userWithExpiry);
        return userWithExpiry;
    }

    fetchUsername(username: string) {
        return this.http.post('MLC.Data.Model.User', { method: 'FetchUsername', controller: 'User' }, { Input: { Username: username.trim() } });
    }

    canRegister(ssn) {
        return this.http.post('MLC.Data.Model.User', { method: 'CanRegister', controller: 'User' }, { Input: { SSN: ssn } });
    }

    canRegisterCustomer(ssn) {
        return this.http.post('MLC.Data.Model.User', { method: 'CanRegisterCustomer', controller: 'User' }, { Input: { SSN: ssn } });
    }

    /**
     *
     * @param {user} User the user
     * @param {store} Store the store for the user
     */
    async glances(user, store) {
        const response = await this.http.post(
            { controller: 'User', method: 'Glances' },
            {
                Input: {
                    User: user,
                    Store: store
                }
            }
        );
        return response;
    }
    async performance() {
        const response = await this.http.post({ controller: 'User', method: 'Performance' }, { });
    return response;
}
    async emailcsm(type, loan) {
        const response = await this.http.post(
            { controller: 'User', method: 'EmailPastDue' },
            {
                Input: {
                    Type: type,
                    Loan: loan
                }
            }
        );
        return response;
    }

    async emailcsrm(type: number, code?: string, storeID?: number, position?: string, rehire?: string, reason?: string, comments?: string) {
        const response = await this.http.post(
            { controller: 'User', method: 'EmailCSRM' },
            {
                Input: {
                    Type: type,
                    Code: code,
                    StoreID: storeID,
                    Position: position,
                    Rehire: rehire,
                    Reason: reason,
                    Comments: comments

                }
            }
        );
        return response;
    }

    async forgotpassword({ username, email }) {
        const response = await this.http.post(
            { controller: 'User', method: 'ForgotPassword' },
            {
                Input: {
                    Username: username,
                    Email: email
                }
            }
        );
        return response;
    }
    async createCredentials(personID, username, password) {
        const response = await this.http.post(
            { controller: 'User', method: 'CreateCredentials' },
            {
                Input: {
                    Person: personID,
                    Username: username,
                    Password: password
                }
            }
        );
        return response;
    }
    async terminateuser(employeeID) {
        const response = await this.http.post(
            { controller: 'User', method: 'TerminateUser' },
            {
                Input: {
                    ID: employeeID
                }
            }
        );
        return response;
    }
}