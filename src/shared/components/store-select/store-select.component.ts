﻿import { forwardRef, Component, Input, Provider } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import StoreService from '../../../services/store.service';
import Store from '../../../../MLC/Types/Store';
import District from '../../../../MLC/Types/District';
export const accessor = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => StoreSelectComponent),
    multi: true
};
@Component({
    selector: 'store-select',
    templateUrl: './store-select.component.html',
    styleUrls: ['./store-select.component.css'],
    providers: [accessor]
})
export default class StoreSelectComponent implements ControlValueAccessor {
    predicate_?: ((store: Store) => {});
    constructor(readonly storeService: StoreService) { }
    set filter(value: ((store: Store) => {}) | undefined) {
        this.predicate_ = value;
    } @Input() get filter() { return this.predicate_; }

    @Input() districts: District[] | undefined;

    async ngOnInit() {
        this.stores = (await this.storeService.fetchCSRStores()) || [];
    }

    get value(): number[] | null {
        return this.value_;
    }
    set value(value_) {
        this.value_ = value_;
        this.onChangeCallback((value_ && value_.join(',')) || '');
    }
    writeValue(value: number[] | null) {
        if (value !== this.value) {
            this.value = value;
        }
    }

    onTouchedCallback = () => { };
    onChangeCallback = _ => { };
    registerOnChange(fn: () => void) {
        this.onChangeCallback = fn;
    }
    registerOnTouched(fn: () => void) {
        this.onTouchedCallback = fn;
    }
    stores?: Store[];
    // tslint:disable-next-line
    value_: number[] | null = null;

}
