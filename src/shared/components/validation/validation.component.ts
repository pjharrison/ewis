import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import ValidationService from '../../services/validation.service';
@Component({
  selector: 'validation',
  templateUrl: './validation.component.html'
})
export default class ValidationComponent {
  @Input() control?: FormControl;
  constructor(readonly validationService: ValidationService) { }

  get errorMessage() {
    if (this.control) {
      for (const [propertyName, error] of Object.entries(this.control.errors || {})) {
        if (this.control.touched || this.control.value) {
          return this.validationService.getErrorMessage(propertyName as any, error);
        }
      }
    }
    return null;
  }
}