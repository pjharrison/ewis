import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import ValidationComponent from './components/validation/validation.component';
import ValidationService from './services/validation.service';
import StoreSelectComponent from './components/store-select/store-select.component';
import FilterPipe from './pipes/filter.pipe';
import FormatPipe from './pipes/format.pipe';
import PreviewPipe from './pipes/preview.pipe';
import MaskPipe from './pipes/mask.pipe';
import TakePipe from './pipes/take.pipe';
import SkipPipe from './pipes/skip.pipe';
import CountdownPipe from './pipes/countdown.pipe';
import SortPipe from './pipes/sort.pipe';
import MinusSignToParensPipe from './pipes/minusSignToParens.pipe';
import ElapsePipe from './pipes/elapse.pipe';
import SafePipe from './pipes/safe.pipe';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbCarouselModule,
        NgbModalModule
    ],
    providers: [ValidationService],
    declarations: [
        ValidationComponent,
        StoreSelectComponent,
        FilterPipe,
        FormatPipe,
        PreviewPipe,
        MaskPipe,
        TakePipe,
        SkipPipe,
        CountdownPipe,
        SortPipe,
        MinusSignToParensPipe,
        ElapsePipe,
        SafePipe
    ],
    exports: [
        ValidationComponent,
        StoreSelectComponent,
        FilterPipe,
        FormatPipe,
        PreviewPipe,
        MaskPipe,
        SkipPipe,
        TakePipe,
        CountdownPipe,
        SortPipe,
        MinusSignToParensPipe,
        ElapsePipe,
        SafePipe
    ]
})
export default class SharedModule { }