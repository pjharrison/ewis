﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'elapse' })
export default class ElapsePipe implements PipeTransform {
    transform(value: string): string {
        if (!value) return value;

        //value is UTC, so set this date as UTC and javascript will localize it:
        var val = new Date(value);
        val = new Date(Date.UTC(val.getFullYear(), val.getMonth(), val.getDate(), val.getHours(), val.getMinutes(), val.getSeconds()));
        //Get Difference:
        var diff = new Date().getTime() - val.getTime();
        var days = Math.floor(diff / (1000 * 60 * 60 * 24));
        diff -= days * (1000 * 60 * 60 * 24);
        var hours = Math.floor(diff / (1000 * 60 * 60));
        diff -= hours * (1000 * 60 * 60);
        var mins = Math.floor(diff / (1000 * 60));
        diff -= mins * (1000 * 60);
        return days + "d " + hours + "hr " + mins + "m ";
    }
}