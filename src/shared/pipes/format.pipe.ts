import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'format' })
export default class FormatPipe implements PipeTransform {
    transform(value: string, kind: 'phone' | 'time' | 'last4' | 'capitalize'): string;
    transform(value: Date, kind: 'date'): string;
    transform(value, kind): string {
        switch (value && kind) {
            case "phone":
                return `(${value.substring(0, 3)}) ${value.substring(3, 6)}-${value.substring(6, 10)}`;
            case "date":
                const dateString = typeof value === 'string' ? value : value.toISOString() 
                //Handle Zero Time Dates differently than "real" dates:
                if (dateString.split('T')[1].replace('Z', '') == "00:00:00") {
                    let result: string[] = dateString.split('T')[0].split('-');
                    return `${result[1]}/${result[2]}/${result[0]}`;
                }

                var d = new Date(dateString);
                d.setMinutes(d.getMinutes() - d.getTimezoneOffset())
                return (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
            case "shortDate":
                const shortDateString = typeof value === 'string' ? value : value.toISOString()
                //Handle Zero Time Dates differently than "real" dates:
                if (shortDateString.split('T')[1].replace('Z', '') == "00:00:00") {
                    let result: string[] = shortDateString.split('T')[0].split('-');
                    return `${result[1]}/${result[2]}`;
                }

                var d = new Date(shortDateString);
                d.setMinutes(d.getMinutes() - d.getTimezoneOffset())
                return (d.getMonth() + 1) + "/" + d.getDate();
            case "time":
                value = new Date(value).toISOString();
                let date = new Date(value);
                let time: string[] = date.toTimeString().split(" ");
                return time[0];
            case "last4":
                if (value.length > 4)
                    return value.substring(value.length - 4);
                return value;
            case "capitalize":
                value = value[0].toUpperCase() + value.substr(1).toLowerCase();
            default:
                return value;
        }
    }
}