﻿import { Pipe, PipeTransform } from '@angular/core';
import sort from '../../utilities/sort';


@Pipe({ name: 'sort', pure: false })
export default class SortPipe implements PipeTransform {
    transform = sort;
}