﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'minusSignToParens' })
export default class MinusSignToParensPipe implements PipeTransform {
    transform(value: string, args?: string): any {
        if (!value) return value;
        return value.charAt(0) === '-' ? '(' + value.substring(1, value.length) + ')' : value;
    }
}