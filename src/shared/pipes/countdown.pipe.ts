﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'countdown' })
export default class CountdownPipe implements PipeTransform {
    transform(value: string, args: number): string {
        if (!value) return value;
        if (!args) args = 90;

        var startDate = new Date(new Date().setDate(new Date().getDate() - args));
        var diff = new Date(value).getTime() - startDate.getTime();

        var days = Math.floor(diff / (1000 * 60 * 60 * 24));
        diff -= days * (1000 * 60 * 60 * 24);
        var hours = Math.floor(diff / (1000 * 60 * 60));
        diff -= hours * (1000 * 60 * 60);
        var mins = Math.floor(diff / (1000 * 60));
        diff -= mins * (1000 * 60);
        if (days == 1)
            return days + " day " + hours + "hrs " + mins + "mins";
        return days + " day " + hours + "hrs " + mins + "mins";
    }
}