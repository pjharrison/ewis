//import 'bootstrap/dist/css/bootstrap.min.css';
import 'element.prototype.matches';
import 'core-js';
import 'zone.js';
import 'whatwg-fetch';
import 'bootstrap';


import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

import AppModule from './app.module';
import env from 'env';

if (env.prod) enableProdMode();
(async function() {
    try {
        await platformBrowserDynamic().bootstrapModule(AppModule);
    } catch (e) {
        console.error(e);
    }
})();
